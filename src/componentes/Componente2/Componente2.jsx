import styles from '../Componente2/Componente2.module.css'
import jaasel from '../../assets/jaasel.png'

function Componente2() {
  return (
    <div className={styles.card} >
        <div className={styles.head}>
            <div className={styles.circle} ></div>
            <div className={styles.img}>
                <img src={jaasel} alt=""/>
            </div>
        </div>

        <div className={styles.description} >
            <h3>JAASEL</h3>
            <h5>Imagina y Crea</h5>
            <p>jaasel es un emprendimiento de accesorios de bisuteria personalizados que tiene como objetivo crear diseños nuevos y creativos para todo tipo de estilos.
            </p>
        </div>

        
    </div>

  );
} export default Componente2;
