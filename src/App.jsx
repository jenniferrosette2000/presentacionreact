import Componente1 from './componentes/componente1/componente1'
import Componente2 from './componentes/componente2/componente2'
import Componente3 from './componentes/componente3/componente3'
import Componente4 from './componentes/componente4/componente4'
import Componente5 from './componentes/componente5/componente5'
import './App.css'

function App() {

  return (
    <>
    <div className='container1'>
      <Componente1/>
    </div>

    <div>
      <Componente2/>
    </div>

    <div>
      <Componente3/>
    </div>

    <div>
    <Componente4/>
    </div>

    <div className='container2'>
    <Componente5/>
    </div>
    </>
  )
}

export default App
