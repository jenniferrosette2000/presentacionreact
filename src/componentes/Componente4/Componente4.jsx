import desing from '../../assets/desing.png'
//import '../Componente4/Componente4.css'
import styles from '../Componente4/Componente4.module.css'
function Componente4() {
  return (
    
    <div className={styles.card} >
        <div className={styles.head}>
            <div className={styles.circle}></div>
            <div className={styles.img} >
                <img src={desing} alt=""/>
            </div>
        </div>

        <div className={styles.description} >
            <h3>DESING</h3>
            <h5>JAR</h5>
            <p>Desing se dedica a la creacion de contenido para redes sociales, presentaciones y por el momemnto imagen de algunos emprendimientos y negocios familiares.
            </p>
        </div>
    </div>


  );
} export default Componente4;
